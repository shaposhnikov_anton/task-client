import { Injectable } from '@angular/core';
import {StompService} from '@stomp/ng2-stompjs';
import {Task} from '../model/task';

@Injectable()
export class TaskService {
  tasks = [];

  constructor(private stompService: StompService) {
    this.createConnectionToBroker();
  }

  static jsonReplacer(key, value) {
    if (key === 'counter') { return undefined; }
    return value;
  }

  createConnectionToBroker() {
    this.stompService.initAndConnect();
    this.addReportConsumer();
  }

  addReportConsumer() {
    this.stompService.subscribe('task-reports', {})
      .subscribe(msg => this.updateExecutedTaskCounter(JSON.parse(msg.body)));
  }

  addTask(taskName: string) {
    const task = new Task(this.tasks.length + 1, taskName);
    this.tasks.push(task);
    this.stompService.publish('/exchange/exchange-tasks', JSON.stringify(task, TaskService.jsonReplacer));

  }

  updateExecutedTaskCounter(msgTask: Task) {
    const task = this.tasks.find(t => t.id === msgTask.id);
    task.counter++;
  }

}
