export class Task {

  id: number;
  name: string;
  counter = 0;

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}
