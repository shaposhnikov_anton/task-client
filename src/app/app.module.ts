import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './shared/modules/material.module';

import { AppComponent } from './app.component';
import { ContainerComponent } from './container/container.component';
import { TaskInputComponent } from './container/task-input/task-input.component';
import { AddTaskButtonComponent } from './container/add-task-button/add-task-button.component';
import { TaskListComponent } from './container/task-list/task-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { TaskService } from './services/task.service';
import { StompConfig, StompService } from '@stomp/ng2-stompjs';

const stompConfig: StompConfig = {
  url: 'ws://127.0.0.1:15674/ws',
  headers: {
    login: 'guest',
    passcode: 'guest'
  },
  heartbeat_in: 0,
  heartbeat_out: 20000,
  reconnect_delay: 5000,
  debug: true
};

@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,
    TaskInputComponent,
    AddTaskButtonComponent,
    TaskListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule
  ],
  providers: [TaskService, StompService, { provide: StompConfig, useValue: stompConfig }],
  bootstrap: [AppComponent]
})
export class AppModule { }
