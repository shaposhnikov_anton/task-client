import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../services/task.service';
import {StompService} from '@stomp/ng2-stompjs';
import {Client} from '@stomp/stompjs';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  tasks = [];

  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.tasks = this.taskService.tasks;
  }

}
