import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TaskService} from '../../services/task.service';
import {MatInput} from '@angular/material';

@Component({
  selector: 'app-task-input',
  templateUrl: './task-input.component.html',
  styleUrls: ['./task-input.component.css']
})
export class TaskInputComponent implements OnInit {
  inputTask = '';

  constructor(private taskService: TaskService) { }

  ngOnInit() {
  }

  processInputValue() {
    if (this.inputTask === '') { return; }
    this.taskService.addTask(this.inputTask);
    this.inputTask = '';
  }

}
