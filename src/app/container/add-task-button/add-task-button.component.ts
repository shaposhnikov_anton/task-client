import {Component, Input, OnInit} from '@angular/core';
import {TaskInputComponent} from '../task-input/task-input.component';

@Component({
  selector: 'app-add-task-button',
  templateUrl: './add-task-button.component.html',
  styleUrls: ['./add-task-button.component.css']
})
export class AddTaskButtonComponent implements OnInit {

  @Input() taskInput: TaskInputComponent;

  constructor() { }

  ngOnInit() {
  }

  addTaskToList() {
    this.taskInput.processInputValue();
  }

}
